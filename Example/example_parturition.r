#rm(list = ls())

library(laserdemog)
library(ggplot2)

# Root for the files directory
# (this root has to be changed by the user)
root <- "D:/Users/Applications/Tools/Demography/Monitoring/Laser/Laserdemog/Doc/Example/"

db <- paste(root, "m_vsplit_ovi_8589.rda", sep = "")
load(db)

### Variations with age

# 1-month age classes 

z <- datv
cellmax <- 8 * 12
z$age <- ifelse(z$cell < cellmax, z$cell, cellmax)
res <- partur(formula = ~ species + sex + age, data = z)
head(res)

z <- res
z$lo <- z$h - 1.96 * z$se.h
z$up <- z$h + 1.96 * z$se.h
p <- ggplot(data = z, aes(x = age, y = h))
p <- p + geom_line(col = "red")
limits <- aes(ymin = z$lo, ymax = z$up)
p <- p + geom_errorbar(limits, width = 0.15, col = "gray50")
p <- p + geom_vline(xintercept = 11, lty = 2, col = "gray50")
p <- p + labs(x = "Age class (1 mo)", y = "Parturition rate (/female-year)")
p <- p + scale_x_continuous(breaks = seq(0, 120, by = 12))
p 

# 1-year age classes 

z <- datv
cellmax <- 8 * 12
z$age <- ifelse(z$cell < cellmax, z$cell, cellmax)
z$age <- trunc(z$age / 12)
res <- partur(formula = ~ species + sex + age, data = z)
res

z <- res
z$lo <- z$h - 1.96 * z$se.h
z$up <- z$h + 1.96 * z$se.h
p <- ggplot(data = z, aes(x = age, y = h))
p <- p + geom_point(col = "red")
limits <- aes(ymin = z$lo, ymax = z$up)
p <- p + geom_hline(yintercept = mean(z$h[z$age >= 1]), lty = 2, col = "gray50")
p <- p + geom_errorbar(limits, width = 0.15, col = "gray50")
p <- p + labs(x = "Age class (1 year)", y = "Parturition rate (/female-year)")
p 

###------------- Variations with season and year

z <- datv
z <- z[z$cell >= 12, ] # adults
res <- partur(formula = ~ species + cycle + phase, data = z)
res$mo <- 1:nrow(res)
head(res)

z <- res
p <- ggplot(data = z, aes(x = mo, y = h))
p <- p + geom_line(col = "red")
p <- p + geom_hline(yintercept = mean(z$h), lty = 2, col = "gray50")
p <- p + scale_x_continuous(breaks = seq(0, max(res$mo), by = 12))
p <- p + labs(x = "Month", y = "Parturition rate (/female-year)")
p

z <- res
p <- ggplot(data = z, aes(x = phase, y = h))
p <- p + facet_wrap(~ cycle, ncol = 5)
p <- p + geom_line(col = "red")
p <- p + geom_hline(yintercept = mean(z$h), lty = 2, col = "gray50")
p <- p + scale_x_continuous(breaks = 1:12)
p <- p + labs(x = "Month", y = "Parturition rate (/female-year)")
p



