# This script presents examples of splitting of demographic Laser data

# References:

# - Lesnoff, M., Lancelot, R., Moulin, C.-H., Messad, S., Juan?s, X., Sahut, C., 2014.
# Calculation of demographic parameters in tropical livestock herds: a discrete time approach
# with laser animal-based monitoring data. Springer, Dordrecht. doi:10.1007/978-94-017-9026-0

# - Lesnoff, M., 2014. Simulating dynamics and productions of tropical
# livestock populations - mmage: A R package for discrete time matrix models.
# CIRAD (French Agricultural Research Centre for International Development), Montpellier, France,
# http://livtools.cirad.fr/mmage. 

#rm(list = ls())
library(laserdemog)

# Root for the files directory
# (this root has to be changed by the user)
root <- "D:/Users/Applications/Tools/Demography/Monitoring/Laser/Laserdemog/Doc/Example/"

#################### IMPORTATION OF THE LASER TABLES

db <- paste(root, "laser_example_ovi.accdb", sep = "")
dat <- importlas(db)

names(dat)

z <- dat$T_HERD
head(z)
str(z)
dim(z)

z <- dat$T_ANIMAL
head(z)
str(z)
dim(z)

z <- dat$T_ENTRY_EXIT
head(z)
str(z)
dim(z)
min(z$datent)
max(z$datexi[!is.na(z$datexi)])

z <- dat$T_PARTURITION
head(z)
str(z)
dim(z)

dat$Ts_typent
dat$Ts_typexi

#################### DEMOGRAPHIC SPLITTING

# In the usual routine of splitting, all the herds of the data base
# are considered in the same time. This routine can become very long
# (and even unfeasible) when the data base is too large.

# Alternatives are to do splitting either herds by herds or
# herd groups by herd groups. For very large data bases, this is
# faster and recommended.

# Below are examples of a monthly horizontal and vertical splitting
# over 5 years (from 01/01/1985 to 31/12/1989).

###-------------- Usual approach (splitting all herds simultaneously)

### Horizontal splitting

nbcycle <- 5 ; nbphase <- 12 ; dstart <- "01/01/1985"
# running time ~ 2 mn
dath <- hsplit(data = dat, nbcycle = nbcycle, nbphase = nbphase, dstart = dstart)
head(dath)
dim(dath)

# Once the data have been split, we can export them in a rda file.
# This allows to save time for further work sessions
# ==> Then, to re-load the data (further work sessions), use function load as below
# saving
db <- paste(root, "m_hsplit_ovi_8589.rda", sep = "")
save(dath, file = db)
# re-loading
#load(db)

### Vertical splitting

datv <- vsplit(data = dat, nbcycle = nbcycle, nbphase = nbphase, dstart = dstart)
head(datv)
dim(datv)

db <- paste(root, "m_vsplit_ovi_8589.rda", sep = "")
save(datv, file = db)
#load(db)

###-------------- Splitting herds by herds (recommended for large data bases)

# define the list of herds to consider
u <- dat$T_HERD$idherd
u
# then data splitting
for(i in 1:length(u)) {   
  print(i)
  print(u[i])
  z <- hsplit(data = dat, nbcycle = nbcycle, nbphase = nbphase, dstart = dstart, listherd = u[i])
  if(i == 1) res <- z else res <- rbind(res, z)
}

###-------------- Splitting herd groups by herd groups (recommended for large data bases)

# define the list of groups of herds to consider
# for instance below for two groups of herds
u <- dat$T_HERD$idherd
u <- list(u[1:150], u[151:238])
u
for(i in 1:length(u)) {   
  print(i)
  print(u[[i]])
  z <- hsplit(data = dat, nbcycle = nbcycle, nbphase = nbphase, dstart = dstart, listherd = u[[i]])
  if(i == 1) res <- z else res <- rbind(res, z)
}



