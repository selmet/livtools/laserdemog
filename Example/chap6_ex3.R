#rm(list = ls())

library(laserdemog)
library(ggplot2)
library(gridExtra)

# Root for the files directory
# (this root has to be changed by the user)
root <- "D:/Users/Applications/Tools/Demography/Monitoring/Laser/Laserdemog/Doc/Example/"

######## DATA IMPORTATION AND SPLITTING

db <- paste(root, "laser_example_ovi.accdb", sep = "")
dat <- importlas(db)
names(dat)

nbcycle <- 1
nbphase <- 1
dstart <- "01/07/1986"
dath <- hsplit(data = dat, nbcycle = nbcycle, nbphase = nbphase, dstart = dstart)

### Saving to rda files (if needed)
# The rda data can then be loaded as below

#save(dath, file = paste(root, "dath_ex3.rda"))

#load(paste(root, "dath_ex3.rda"))

######## IMPORTATION OF THE DEWORMING DATA AND MERGING WITH THE SPLIT DATA

db <- paste(root, "prophyl8687.csv", sep = "")
tprophyl <- read.table(file = db, header = TRUE, sep = ";")
head(tprophyl)

table(tprophyl$typ)

z <- dath[dath$cell == 0, ]
z <- merge(z, tprophyl, by = "idherd")
dath.final <- z
head(dath.final)

z <- dath.final[dath.final$ini == 1, ]
table(z$sex, z$typ)

######## CALCULATIONS

res <- exit(formula = ~ cell + sex + typ, data = dath.final, event = "DEA")
res

u <- res
u$lo <- u$h - 1.96 * u$se.h
u$up <- u$h + 1.96 * u$se.h
u
limits <- aes(ymax = u$up, ymin = u$lo)
p <- ggplot(data = u, aes(x = typ, y = h))
p <- p + facet_wrap(~ sex, ncol = 2)
p <- p + geom_errorbar(limits, width = 0.15, col = "grey50")
p <- p + geom_point(shape = 21, size = 3, col = "red", fill = "white", stroke = 2)
p <- p + labs(x = "Herd group", y = "Death rate (/animal-year)")
p

z <- res
z$pc1 <- round(1 - exp(-z$h), digits = 3)
z

res.off <- exit(formula = ~ cell + sex + typ, data = dath.final, event = c("OFF", "END", "WIT"))
res.off

exit(formula = ~ cell + sex + typ, data = dath.final, event = "EXI")

z$ncens <- res.off$nbevent
z$nc <- round(z$n - z$ncens / 2)
z

z$pc2 <- round(res$nbevent / z$nc, digits = 3)
z

### logit for pc2

fm1 <- glm(
    formula = cbind(nbevent, nc - nbevent) ~ sex + typ + sex:typ,
    data = z,
    family = binomial 
    ) 
summary(fm1)
anova(fm1, test = "Chisq")

fm2 <- glm(
    formula = cbind(nbevent, nc - nbevent) ~ sex + typ,
    data = z,
    family = binomial 
    ) 

fm3 <- glm(
    formula = cbind(nbevent, nc - nbevent) ~ typ,
    data = z,
    family = binomial 
    ) 

AIC(fm1, fm2, fm3)

z <- dath.final
z <- exit(formula = ~ cell + idherd + typ, data = z, event = "DEA")
z$ncens <- exit(formula = ~ cell + idherd + typ, data = dath.final, event = c("OFF", "END", "WIT"))$nbevent
z$nc <- round(z$n - z$ncens / 2)
z$pc2 <- round(z$nbevent / z$nc, digits = 3)
head(z)
p <- ggplot(data = z, aes(x = typ, y = pc2, col = typ))
p <- p + geom_point() + geom_jitter(width = 0.15)
p <- p + labs(x = "Herd group", y = "Death probability")
p

### log-linear for h

fm1 <- glm(
    formula = nbevent ~ sex + typ + sex:typ,
    data = res,
    offset = log(trisk),
    family = poisson 
    ) 
summary(fm1)
anova(fm1, test = "Chisq")

fm2 <- glm(
    formula = nbevent ~ sex + typ,
    data = res,
    offset = log(trisk),
    family = poisson 
    ) 

fm3 <- glm(
    formula = nbevent ~ typ,
    data = res,
    offset = log(trisk),
    family = poisson 
    ) 

AIC(fm1, fm2, fm3)


