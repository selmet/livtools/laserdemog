abort <-
function(formula, data, digits = 3, ini = TRUE, end = FALSE, timecell = "year") {

  if(nrow(data) == 0)
    stop("\n The data frame is empty. \n\n")
   
  CALL <- match.call()

  tmp <- as.data.frame(lapply(data, function(x) if(mode(x) == "character") factor(x) else x))
  
  tmp <- tmp[tmp$sex == "F", ]
  tmp$sex <- factor(tmp$sex, levels = "F")

  # binary indicating the presence of the animal at end of the cell
  tmp$end <- ifelse(is.na(tmp$typexi), 1, 0)

  if(ini) tmp <- tmp[tmp$ini == 1, ]
  if(end) tmp <- tmp[tmp$end == 1, ]

  tmp$imi <- ifelse(tmp$ini == 0, 1, 0)  
  tmp$n <- tmp$ini + tmp$imi

  if(timecell == "day") tmp$trisk <- tmp$trisk
  if(timecell == "week") tmp$trisk <- tmp$trisk / 7.02
  if(timecell == "month") tmp$trisk <- tmp$trisk / 30.42
  if(timecell == "year") tmp$trisk <- tmp$trisk / 365
   
  tmp$nbevent <- tmp$nbabor

  f <- formula
  if(f[[2]] == 1) {
    tmp$V1 <- rep(1, nrow(tmp))
    f <- formula(~ V1)
  }

  tab <- zagg(formula = formula(paste("n ~", f[2])), data = tmp, FUN = sum)
  names(tab)[ncol(tab)] <- "n"
  tab$trisk <- zagg(formula = formula(paste("trisk ~", f[2])), data = tmp, FUN = sum)$x.agg
  tab$nbevent <- zagg(formula = formula(paste("nbevent ~", f[2])), data = tmp, FUN = sum)$x.agg
  
  tab$p <- tab$nbevent / tab$n
  tab$se.p <- (tab$p * (1 - tab$p) / tab$n)^0.5 
  tab$h <- tab$nbevent / tab$trisk
  tab$se.h <- (tab$h / tab$trisk)^0.5   
  tab$trisk <- round(tab$trisk, digits = digits)

  z <- (ncol(tab) - 3):ncol(tab)
  tab[, z] <- round(tab[, z], digits = digits)
  row.names(tab) <- 1:nrow(tab)
  
  tab

}
