zagg <-
function(formula, data, FUN) {
  
#----------------------------------------------------------------------------------------------------------

  agglength <- function(by, sort.it) {
  if(sort.it) {
    ord <- do.call("order", unname(by))
    by <- by[ord,  ]
  }
  logical.diff <- function(group) group[-1] != group[ - length(group)]
  change <- logical.diff(by[[1]])
  for(i in seq(along = by)[-1])
    change <- change | logical.diff(by[[i]])
  by <- by[c(TRUE, change),  , drop = FALSE]
  by$x.agg <- diff(c(0, seq(len = length(change) + 1)[c(change, TRUE)]))
  by
  }

  aggsum <- function(x, by, sort.it){
  if(sort.it) {
    ord <- do.call("order", unname(by))
    x <- x[ord]
    by <- by[ord,  ]
    }
  logical.diff <- function(group) group[-1] != group[ - length(group)]
  change <- logical.diff(by[[1]])
  for(i in seq(along = by)[-1])
    change <- change | logical.diff(by[[i]])
  by <- by[c(TRUE, change),  , drop = FALSE]
  by$x.agg <- diff(c(0, cumsum(x)[c(change, TRUE)]))
  by
  }

#----------------------------------------------------------------------------------------------------------
 
  if(nrow(data) == 0)
    stop("\n The data frame is empty. \n\n")
  
  tmp <- as.data.frame(lapply(data, function(x) if(mode(x) == "character") factor(x) else x))

  fname <- as.character(substitute(FUN))
  if(!(fname %in% c("length", "sum")))
    stop("\n Argument FUN must be length or sum. \n\n")
  
  listvar <- attr(terms(formula), "term.labels")
  nbvar <- length(listvar)

  # Functions agg.length and agg.sum do not work with only one covariable
  if(nbvar == 1)
    listvar <- c(listvar, listvar)
    
  if(fname == "sum")
    x <- data[, all.vars(formula)[1]]
  
  by <- data[, listvar] 

  ### Outputs

  if(fname == "length")
    tab <- agglength(by, sort.it = TRUE)

  if(fname == "sum")
    tab <- aggsum(x, by, sort.it = TRUE)
  
  if(nbvar == 1)
      tab <- tab[, -2]
  
  tab

}
