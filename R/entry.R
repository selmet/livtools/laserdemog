entry <-
function(formula, data, digits = 3, ini = TRUE, timecell = "year", event = "ALL") {

  if(nrow(data) == 0)
    stop("\n The data frame is empty. \n\n")

  CALL <- match.call()

  tmp <- as.data.frame(lapply(data, function(x) if(mode(x) == "character") factor(x) else x))

  if(timecell == "day") tmp$trisk <- tmp$trisk
  if(timecell == "week") tmp$trisk <- tmp$trisk / 7.02
  if(timecell == "month") tmp$trisk <- tmp$trisk / 30.42
  if(timecell == "year") tmp$trisk <- tmp$trisk / 365

  # Tsys_typent
  #  typent                                               label
  #    BIR                                                Birth
  #    PUR                                             Purchase
  #    BAR                                               Barter
  #    GIF                             Gift, dowry, inheritance
  #    ARC       Arrival for contract (entrustment, loan, etc.)
  #    CBC  Coming back from contract (entrustment, loan, etc.)
  #    BEG                              Beginning of monitoring
  #    UNK                                              Unknown  
  
  # additionnal codes for event
  zevent <- event
  for(i in 1:length(event)) {
    if("COM" %in% event) zevent <- c(zevent,        "PUR", "BAR"                            )
    if("CON" %in% event) zevent <- c(zevent,                             "ARC", "CBC"       )
    if("INT" %in% event) zevent <- c(zevent,        "PUR", "BAR", "GIF", "ARC", "CBC"       )
    if("ALL" %in% event) zevent <- c(zevent, "BEG", "PUR", "BAR", "GIF", "ARC", "CBC", "UNK")
  }
  zevent <- unique(zevent)

  # nbevent
  tmp$nbevent <- ifelse(tmp$typent %in% zevent, 1, 0)

  # n and trisk
  tmp$imi <- ifelse(tmp$ini == 0, 1, 0)

  if(ini) {
	  tmp$n <- tmp$ini
	  tmp$trisk <- ifelse(tmp$ini == 1, tmp$trisk, 0)
  }
	
  if(!ini) tmp$n <- tmp$ini + tmp$imi
	
  # tab
  f <- formula
	  #f <- formula(~ sex + cell)
  if(f[[2]] == 1) {
  tmp$V1 <- rep(1, nrow(tmp))
  f <- formula(~ V1)
  }

  tab <- zagg(formula = formula(paste("n ~", f[2])), data = tmp, FUN = sum)
  names(tab)[ncol(tab)] <- "n"
  tab$trisk <- zagg(formula = formula(paste("trisk ~", f[2])), data = tmp, FUN = sum)$x.agg
  tab$nbevent <- zagg(formula = formula(paste("nbevent ~", f[2])), data = tmp, FUN = sum)$x.agg
  tab
	
  tab$p <- tab$nbevent / tab$n
  tab$se.p <- (tab$p * (1 - tab$p) / tab$n)^0.5 
  tab$h <- tab$nbevent / tab$trisk
  tab$se.h <- (tab$h / tab$trisk)^0.5 
  tab$trisk <- round(tab$trisk, digits = digits)

  z <- (ncol(tab) - 3):ncol(tab)
  tab[, z] <- round(tab[, z], digits = digits)
  row.names(tab) <- 1:nrow(tab)

  tab

}
