.onAttach <- function(libname, pkgname){
    packageStartupMessage(
      "Package laserdemog version : 1.3-0 \n\n 
      --- Please check if there is a new version \n
      --- at URL https://gitlab.cirad.fr/selmet/livtools/laserdemog/ \n"
      )
}
