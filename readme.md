## Description

laserdemog is a R package using a discrete-time approach for estimating demographic parameters in livestock herds from animal-based monitoring Laser data.

## Installation

The easiest way is to download [here](https://gitlab.cirad.fr/selmet/livtools/laserdemog/-/releases) the latest version of the source package from the release section of this repository.

Maybe you prefer install the package laserdemog directly from the remote repository gitlab, you need to ensure that you have Git software installed on your system. More details [here](https://docs.posit.co/ide/user/ide/guide/tools/version-control.html) on how to install the versioning tool Git in [RStudio](https://posit.co/download/rstudio-desktop/)

Then you use install_git from the package [remotes.](https://CRAN.R-project.org/package=remotes) 


```R
install.packages("remotes")
library(remotes)
```

for the current developing version:
```R
remotes::install_git('https://gitlab.cirad.fr/selmet/livtools/laserdemog')
```
or for a specific version:
```R
remotes::install_git('https://gitlab.cirad.fr/selmet/livtools/laserdemog', ref = "1.3.1")
```

## Documentation

There is a manual available [here](https://gitlab.cirad.fr/selmet/livtools/laserdemog/-/blob/master/doc/laserdemog_manual%202017-12.pdf) that briefly summarises the demographic concepts developed in the handbook mentioned above and gives a detailed presentation of the laserdemog package of functions for calculating demographic parameters for discrete-time data.

## Bibliography

A handbook accompanies thes package describing the general theory behind calculation of the demographic parameters, in particular concepts such as probability, hazard rate, demographic interferences or competing risks. we highly recommend to have a look to this handbook:

[Lesnoff, M., Lancelot, R., Moulin, C.-H., Messad, S., Juanès, X., Sahut, C., 2014](http://link.springer.com/book/10.1007/978-94-017-9026-0). Calculation of demographic parameters in tropical livestock herds: a discrete time approach with laser animal-based monitoring data. Springer, Dordrecht.

## Support

Please send a message to the maintainer: Julia Vuattoux <julia.vuattoux@cirad.fr>

## License
[GPL (>= 3)](https://www.gnu.org/licenses/gpl-3.0.html)
